*** Settings ***
Resource    ../resources/login_keywords.robot
Library  Browser
Library  String
Library  DateTime
#Library    SeleniumLibrary
Suite Setup     Run Keywords
...             Browser.New Browser
...             browser=${BROWSER}
...             headless=${HEADLESS}  AND
...             Set Library Search Order  Browser  SeleniumLibrary     AND
...             Browser.Set Browser Timeout  1m
Test Teardown  Test Teardown Keyword




*** Variables ***
${browser}=  Chromium
${headless}=  False

*** Test Cases ***
TC1 Prihlasenie do aplikacie
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera

    Otvor browser
