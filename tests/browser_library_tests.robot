*** Settings ***
Resource    ../resources/browser_library_keywords.robot
Library  Browser
Library  String
Library  DateTime
Library    SeleniumLibrary
Suite Setup     Run Keywords
...             Browser.New Browser
...             browser=${BROWSER}
...             headless=${HEADLESS}    AND
...             Set Library Search Order  Browser  SeleniumLibrary     AND
...             Browser.Set Browser Timeout  1m
Test Teardown  Test Teardown Keyword




*** Variables ***
${browser}=  chromium
${headless}=  False

*** Test Cases ***
TC1 A/B Testing Element
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie prveho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    text=A/B Testing
    Skontroluj Zmenu Obrazovky    text=A/B Test

TC2 Add/Remove Elements
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie druheho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    text=Add/Remove Elements
    Najdi Element A Klikni   text=Add Element
    Najdi Element A Klikni   text=Delete

TC3 Basic Auth
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie tretieho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    text=Basic Auth
    Najdi Element A Klikni   text=Add Element
    Najdi Element A Klikni   text=Delete