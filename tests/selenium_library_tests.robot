*** Settings ***
Resource    ../resources/selenium_library_keywords.robot
Library    SeleniumLibrary
Test Teardown    Close Browser

*** Test Cases ***
TC1 A/B Testing Element
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie prveho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[1]/a
    Skontroluj Zmenu Obrazovky    xpath://*[@id="content"]/div/h3

TC2 Add/Remove Elements
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie druheho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[2]/a
    Klikni Na Button    xpath://*[@id="content"]/div/button
    Klikni Na Button   xpath://*[@id="elements"]/button
    Skontroluj Neviditelnost Elementu    xpath://*[@id="elements"]/button

TC3 A/B Testing Element
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie prveho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[3]/a
    Skontroluj Zmenu Obrazovky    xpath://*[@id="content"]/div/h3

TC4 Basic Auth
    [Tags]  skuska
    [Documentation]    Test na otvorenie browsera a pouzitie prveho testovacieho linku

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[3]/a
    Prihlas sa    admin    admin
    Sleep    5s

TC5 Broken Images
    [Tags]  skuska
    [Documentation]    Check if the user image is ok

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[4]/a
    Skontroluj obrazok    //*[@id="content"]/div/img[1]
    Sleep    5s

TC6 Challenging DOM blue button
    [Tags]    skuska
    [Documentation]    blue button

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Najdi element a porovnaj    xpath://div[@class="large-2 columns"]/a[@class="button"]
    
TC7 Challenging DOM red button    
    [Tags]    skuska
    [Documentation]    red button

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Najdi element a porovnaj    xpath://div[@class="large-2 columns"]/a[@class="button alert"]

TC8 Challenging DOM green button
    [Tags]    skuska
    [Documentation]    green button

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Najdi element a porovnaj    xpath://div[@class="large-2 columns"]/a[@class="button success"]

TC9 Challenging DOM headers
    [Tags]    skuska
    [Documentation]    check table headers

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[1]    Lorem
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[2]    Ipsum
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[3]    Dolor
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[4]    Sit
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[5]    Amet
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[6]    Diceret
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/thead/tr/th[7]    Action

TC10 Challenging DOM first column
    [Tags]    skuska
    [Documentation]    check  first table columns

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[1]/td[1]    Iuvaret0
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[2]/td[1]    Iuvaret1
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[3]/td[1]    Iuvaret2
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[4]/td[1]    Iuvaret3
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[5]/td[1]    Iuvaret4
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[6]/td[1]    Iuvaret5
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[7]/td[1]    Iuvaret6
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[8]/td[1]    Iuvaret7
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[9]/td[1]    Iuvaret8
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[10]/td[1]    Iuvaret9

TC11 Challenging DOM second column
    [Tags]    skuska
    [Documentation]    check  second table columns

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[1]/td[2]    Apeirian0
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[2]/td[2]    Apeirian1
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[3]/td[2]    Apeirian2
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[4]/td[2]    Apeirian3
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[5]/td[2]    Apeirian4
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[6]/td[2]    Apeirian5
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[7]/td[2]    Apeirian6
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[8]/td[2]    Apeirian7
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[9]/td[2]    Apeirian8
    Skontroluj Tabulku    xpath://div[@class="large-10 columns"]/table/tbody/tr[10]/td[2]    Apeirian9

TC12 Challenging DOM edit second line
    [Tags]    skuska
    [Documentation]    edit second line

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Najdi Element A Klikni    xpath://div[@class="large-10 columns"]/table/tbody/tr[2]/td[7]/a[1]
    Najdi Element A Porovnaj    xpath://div[@class="large-10 columns"]/table/tbody/tr[2]/td[7]/a[1]

TC13 Challenging DOM delete sixth line
    [Tags]    skuska
    [Documentation]    delete sixth line

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[5]/a
    Najdi Element A Klikni    xpath://div[@class="large-10 columns"]/table/tbody/tr[6]/td[7]/a[2]
    Najdi Element A Porovnaj    xpath://div[@class="large-10 columns"]/table/tbody/tr[6]/td[1]

TC14 Checkboxes
    [Tags]    skuska
    [Documentation]    checkboxes

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[6]/a
    Skontroluj Checkboxy    xpath://*[@id="checkboxes"]/input[1]    //*[@id="checkboxes"]/input[2]

TC15 Context Menu
    [Tags]    skuska
    [Documentation]    check context menu

    Otvor browser
    Skontroluj Spravnu URL
    Najdi Element A Klikni    xpath://*[@id="content"]/ul/li[7]/a
    Skontroluj Checkboxy    xpath://*[@id="checkboxes"]/input[1]    //*[@id="checkboxes"]/input[2]
