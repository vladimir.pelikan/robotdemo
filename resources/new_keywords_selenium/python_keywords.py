try:
    from robot.libraries.BuiltIn import BuiltIn
    from robot.libraries.BuiltIn import _Misc
    import robot.api.logger as logger
    from robot.api.deco import keyword
    ROBOT = False
except Exception:
    ROBOT = False


@keyword("GENERATE BASIC AUTH URL")
def basicAuthURL(link, user, password):
    parts_lst = link.partition("://")
    link = "{}{}{}:{}@{}".format(parts_lst[0], parts_lst[1], user, password, parts_lst[2])
    print(link)
    return link