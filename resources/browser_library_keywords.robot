*** Settings ***
Library  SeleniumLibrary
Library  Browser
Library  String
Library  DateTime
#Resource  ../BrowserLibrary/login_browser.robot

*** Variables ***
${baseUrl}=  https://the-internet.herokuapp.com/

*** Keywords ***
Test Teardown Keyword
    [Documentation]  Po skonceni testu ukonci
    Close Browser


Otvor browser
    [Documentation]    Otvor Browser na URLke

    ${viewport}=  Create Dictionary   height=1920   width=1080
    Browser.New Context     acceptDownloads=${True}     viewport=${viewport}    #httpCredentials=&{credentials}
    Browser.New Page    ${baseURL}
    Sleep   5s
    Log to console   Otvoril som browser

Skontroluj spravnu URL
    [Documentation]    Smontroluj spravnost URL

    ${actualURL}=    Browser.Get Url
    Should Be Equal As Strings    ${actualURL}    ${baseURL}
    Sleep   5s

Najdi Element A Klikni
    [Arguments]    ${element}

    Browser.Get Element    ${element}
    Browser.Click    ${element}
    Sleep    5s
    
Skontroluj Zmenu Obrazovky
    [Arguments]    ${element}

    Browser.Get Element    ${element}
    
Klikni Na Button
    [Arguments]    ${element}

    Browser.Get Element    ${element}
    Browser.Click    ${element}
    Sleep    5s


