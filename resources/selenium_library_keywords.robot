*** Settings ***
Library  SeleniumLibrary
Library    ../resources/new_keywords_selenium/python_keywords.py

*** Variables ***
${baseUrl}=  https://the-internet.herokuapp.com/
${link_auth}    xpath://a[contains(text(), 'Basic Auth')]
${Browser}    chrome

*** Keywords ***
Test Teardown Keyword
    [Documentation]  Po skonceni testu ukonci
    Close Browser

Otvor browser
    [Documentation]    Otvor Browser na URLke

    Open Browser    ${baseURL}    ${Browser}    -inprivate
    Sleep   5s

Skontroluj spravnu URL
    [Documentation]    Smontroluj spravnost URL

    ${actualURL}=    Get Location
    Should Be Equal As Strings    ${actualURL}    ${baseURL}
    Sleep   5s

Najdi Element A Klikni
    [Arguments]    ${element}

    Element Should Be Visible   ${element}
    Click Element   ${element}
    Sleep    5s

Skontroluj Zmenu Obrazovky
    [Arguments]    ${element}

    Element Should Be Visible   ${element}

Klikni Na Button
    [Arguments]    ${element}

    Element Should Be Visible   ${element}
    Click Button    ${element}
    Sleep    5s
    
Skontroluj neviditelnost elementu
    [Arguments]    ${element}
    
    Element Should Not Be Visible    ${element}

Prihlas sa
    [Arguments]    ${user}    ${pw}
    ${link_auth}    GENERATE BASIC AUTH URL    ${link_auth}    ${user}    ${pw}
    Go To    ${link_auth}

Skontroluj obrazok
    [Arguments]    ${element}

    Page Should Contain Image    ${element}
    Element Should Be Visible    ${element}
    ${src}=    Get Element Attribute    ${element}     src
    Go To    ${src}
    Wait Until Element Is Visible    css: img
    Go Back
    
Najdi element a porovnaj
    [Arguments]    ${element}
    
    ${id_1}=    Get Element Attribute   ${element}    text
    Click Element    ${element}
    ${id_2}=    Get Element Attribute   ${element}    text
    Should Not Be Equal    ${id_1}    ${id_2}

Skontroluj tabulku
   [Arguments]    ${element}    ${headers}

   Element Text Should Be    ${element}    ${headers}
   Sleep    2s
   
Skontroluj Checkboxy
    [Arguments]    ${used_checkbox}    ${unused_checkbox}
    
    Checkbox Should Not Be Selected    ${used_checkbox}
    Checkbox Should Be Selected    ${unused_checkbox}
    Sleep    3s
    